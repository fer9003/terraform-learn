provider "aws" {
    region = "us-east-2"
    
}

#VARIABLES
variable "cidr_blocks" {
    description = " cidr block and tags for vpc and subnet"
    type = list(object({
        cidr_block = string
        name = string
    }))
}

variable avail_zone {}

#Create VPC
resource "aws_vpc" "development-vpc" {
    cidr_block = var.cidr_blocks[0].cidr_block

    tags = {
        Name = var.cidr_blocks[0].name
    }
}

#Create Subnet
resource "aws_subnet" "dev-subnet-1" {
    vpc_id = aws_vpc.development-vpc.id
    cidr_block = var.cidr_blocks[1].cidr_block
    availability_zone = var.avail_zone

    tags = {
        Name = cidr_blocks[1].name
    }
}

output "dev-vpc-id" {
    value = aws_vpc.development-vpc.id
}

output "dev-subnet-id" {
    value = aws_subnet.dev-subnet-1.id
}

/* data "aws_vpc" "existing_vpc" {
    default = true
}

resource "aws_subnet" "default_subnet-4" {
    vpc_id = data.aws_vpc.existing_vpc.id
    cidr_block = "172.31.48.0/20"
    availability_zone = "us-east-2c"

    tags = {
        Name = "subnet4"
    }
} */

